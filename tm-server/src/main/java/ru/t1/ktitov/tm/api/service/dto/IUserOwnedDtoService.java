package ru.t1.ktitov.tm.api.service.dto;

import ru.t1.ktitov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M>, IUserOwnedDtoRepository<M> {
}
